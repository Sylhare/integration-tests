# Integration

Trying integration between gitlab and other stuff. <br>
This project is also used for tests in my other [gitlab_stats](https://github.com/sylhare/gitlab_stats) project: 
   - A python script to get gitlab stats from the command line.

## Gitlab

- project id: 4895805
- gitlab api: https://docs.gitlab.com/ee/api/

```bash
# to get my project info
curl --header "Private-Token: 9koFAKEe1gTOKENiJp8" https://gitlab.com/api/v4/projects/4895805

# To test gitlab public api
curl https://gitlab.com/api/v4/projects/
```

## Slack

- Slack test: https://toast-b.slack.com
- Integration procedure: https://docs.gitlab.com/ee/user/project/integrations/slack.html
